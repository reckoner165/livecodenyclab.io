---
name: Kairos
image: https://github.com/Leofltt/Kairos/blob/master/pics/kairos.png
link: https://github.com/Leofltt/Kairos
---

An [Haskell](https://www.haskell.org/) library for music composition and performance using [Csound](https://csound.com/).
